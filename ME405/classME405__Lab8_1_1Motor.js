var classME405__Lab8_1_1Motor =
[
    [ "__init__", "classME405__Lab8_1_1Motor.html#a773aeacf43c851adcace9f95a2056f40", null ],
    [ "disable", "classME405__Lab8_1_1Motor.html#afaa12853e9e27c3b35b090b3221c4c7e", null ],
    [ "enable", "classME405__Lab8_1_1Motor.html#abb4fceb9f726d7c7f5b7236d3f985bc9", null ],
    [ "Error", "classME405__Lab8_1_1Motor.html#a7bb61c74edbc3435e5180bd25d40ddd8", null ],
    [ "ISR", "classME405__Lab8_1_1Motor.html#a59babe450b43fbb661910f50830d747e", null ],
    [ "set_duty", "classME405__Lab8_1_1Motor.html#af9296dd48db6ee0457975b415a2bef01", null ],
    [ "dbg", "classME405__Lab8_1_1Motor.html#a77e9a8016aeb4be0e3a8e0e8f82ddff6", null ],
    [ "ExtInterrupt", "classME405__Lab8_1_1Motor.html#a4796cffc5e91269e25335a765c7dd299", null ],
    [ "IN1", "classME405__Lab8_1_1Motor.html#a34ce355d7f4e70f672580bc749586da2", null ],
    [ "IN2", "classME405__Lab8_1_1Motor.html#aa376f59a21cdaba998bd742eef2d4fac", null ],
    [ "nFault", "classME405__Lab8_1_1Motor.html#adcda394ec26bf1398238472a9474220c", null ],
    [ "pin15", "classME405__Lab8_1_1Motor.html#a880c20d917d7b3729b1a7b0ea73d7a26", null ],
    [ "timer", "classME405__Lab8_1_1Motor.html#a45ba2216a0f1ac6f0ce41616b1b7edfe", null ],
    [ "timer_ch_IN1", "classME405__Lab8_1_1Motor.html#ad75cd7ebcf56a76634e837787501d28d", null ],
    [ "timer_ch_IN2", "classME405__Lab8_1_1Motor.html#ae5d4662bc351745da1550cd05f0bef73", null ]
];