var classLab7__I2CDriver_1_1BNO055 =
[
    [ "__init__", "classLab7__I2CDriver_1_1BNO055.html#a736e8b500a0f1fb1ff2858102c0027bb", null ],
    [ "calibration", "classLab7__I2CDriver_1_1BNO055.html#aeadd76b7a73a81d37ab54c5dd3186d6c", null ],
    [ "address", "classLab7__I2CDriver_1_1BNO055.html#a86ed814f3775307e8cfa97d166c2d3b5", null ],
    [ "Calibration_Reg", "classLab7__I2CDriver_1_1BNO055.html#a02d3d65066afd90f12c67eda30552b40", null ],
    [ "Chip_ID", "classLab7__I2CDriver_1_1BNO055.html#a7602277941a0b397f1b6c69a21400ffc", null ],
    [ "i2c", "classLab7__I2CDriver_1_1BNO055.html#abd43a19c3d2543535aeabc7662a0148e", null ],
    [ "ID_Reg", "classLab7__I2CDriver_1_1BNO055.html#abc7a04648868f15296a44dde068f67c9", null ],
    [ "SCL", "classLab7__I2CDriver_1_1BNO055.html#a9b58fe58dae5eb188bbacb6cc6aae73c", null ],
    [ "SDA", "classLab7__I2CDriver_1_1BNO055.html#a102a077ec6b0f96aa338b5f8a3dd1dd2", null ]
];