var annotated_dup =
[
    [ "Lab4_mcp9808", "namespaceLab4__mcp9808.html", "namespaceLab4__mcp9808" ],
    [ "Lab7_FTPDriver", "namespaceLab7__FTPDriver.html", "namespaceLab7__FTPDriver" ],
    [ "Lab7_I2CDriver", "namespaceLab7__I2CDriver.html", "namespaceLab7__I2CDriver" ],
    [ "Lab8_MotorEncoderDriver", "namespaceLab8__MotorEncoderDriver.html", "namespaceLab8__MotorEncoderDriver" ],
    [ "ME405_Drivers", "namespaceME405__Drivers.html", "namespaceME405__Drivers" ],
    [ "ME405_Lab7", "namespaceME405__Lab7.html", "namespaceME405__Lab7" ],
    [ "ME405_Lab8", "namespaceME405__Lab8.html", "namespaceME405__Lab8" ],
    [ "ME405_Touchscreen", "namespaceME405__Touchscreen.html", "namespaceME405__Touchscreen" ]
];