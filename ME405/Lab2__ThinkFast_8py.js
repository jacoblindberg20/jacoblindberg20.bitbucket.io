var Lab2__ThinkFast_8py =
[
    [ "calc_time", "Lab2__ThinkFast_8py.html#a49a63851eb122a0454d1d297657bc55c", null ],
    [ "count_isr", "Lab2__ThinkFast_8py.html#aacba39a6695ca3c6fe72ac0fd8e9a735", null ],
    [ "avg", "Lab2__ThinkFast_8py.html#abe23679bdda4fb95340e5e184c695b6a", null ],
    [ "delta", "Lab2__ThinkFast_8py.html#a5a85d05a5387238e8772be9a1c1b5d48", null ],
    [ "end_time", "Lab2__ThinkFast_8py.html#a02e5b49c33b8a8dd1f090369824881cf", null ],
    [ "Exec", "Lab2__ThinkFast_8py.html#aeb7f8c22efb1c83f4ea473d85d071aec", null ],
    [ "extint", "Lab2__ThinkFast_8py.html#a0ba89677b5e12bfc8cda2a4d68492163", null ],
    [ "myLED", "Lab2__ThinkFast_8py.html#a1671b27d535e08cca79579cbf21e8b3a", null ],
    [ "random_delay", "Lab2__ThinkFast_8py.html#a4ac0c18d9e016cc30380a299feb30873", null ],
    [ "reac_array", "Lab2__ThinkFast_8py.html#ae1f64daad7840359cd6e8c1b58018894", null ],
    [ "start_time", "Lab2__ThinkFast_8py.html#ab4e0ed0e947b19040bf313b3b2976bb0", null ],
    [ "t1", "Lab2__ThinkFast_8py.html#a9faa5a47b2c4af2c10c0fc579b899e45", null ],
    [ "time_begin", "Lab2__ThinkFast_8py.html#a698895b73a90be45657684bfa43c4440", null ],
    [ "time_current", "Lab2__ThinkFast_8py.html#a7fe0e9a8b64c3ca628354f57c142a9c7", null ],
    [ "time_end", "Lab2__ThinkFast_8py.html#a3034cc715d735b33161eb1a623bf3f56", null ]
];