var classME405__Lab7_1_1TouchScreen =
[
    [ "__init__", "classME405__Lab7_1_1TouchScreen.html#ac854dd804e74f3b4a2efb7bc7460d234", null ],
    [ "Optimized_Scan_Y", "classME405__Lab7_1_1TouchScreen.html#a520092de9e49bd7b8ee99b0112b7755e", null ],
    [ "Scan_X", "classME405__Lab7_1_1TouchScreen.html#a4594789755d726eecd2da12071e807d2", null ],
    [ "Scan_XYZ", "classME405__Lab7_1_1TouchScreen.html#a2fb57225b60cce9bf2786755ba3a42df", null ],
    [ "Scan_Y", "classME405__Lab7_1_1TouchScreen.html#a0a41ba2cbdc45f2440581c522cc68410", null ],
    [ "Scan_Z", "classME405__Lab7_1_1TouchScreen.html#a60daed45a87663d8943ae00f9d4aa704", null ],
    [ "pin_xm", "classME405__Lab7_1_1TouchScreen.html#aa5ef71dfda1d45a0d5a4811e9d153fd9", null ],
    [ "pin_xp", "classME405__Lab7_1_1TouchScreen.html#a1fce40a53fecb45cc6965007a7d25f09", null ],
    [ "pin_ym", "classME405__Lab7_1_1TouchScreen.html#a78a404ca24c202ea71f6eaf63d08252a", null ],
    [ "pin_yp", "classME405__Lab7_1_1TouchScreen.html#a773548db0e2dc056865a24b263b4da6e", null ],
    [ "scan_tup", "classME405__Lab7_1_1TouchScreen.html#aee7deddd1aabc0283c573e000a52f2c5", null ],
    [ "X_convert", "classME405__Lab7_1_1TouchScreen.html#a15f04c2657aac591051a890f00c7f95d", null ],
    [ "xc", "classME405__Lab7_1_1TouchScreen.html#a7ff131878f20f8adba956053ca3a7e4a", null ],
    [ "Y_convert", "classME405__Lab7_1_1TouchScreen.html#a2190d2c2295547239fa6f5edddb2c04d", null ],
    [ "yc", "classME405__Lab7_1_1TouchScreen.html#a0b6a658b1bda6123c45ad44f100b76fd", null ]
];