var classFTPDriver_1_1TouchScreen =
[
    [ "__init__", "classFTPDriver_1_1TouchScreen.html#adb639a4f2383d3107953ffac96983c27", null ],
    [ "Optimized_Scan_Y", "classFTPDriver_1_1TouchScreen.html#aa8a6e112746f54a4378df28da91041b9", null ],
    [ "Scan_X", "classFTPDriver_1_1TouchScreen.html#a3a382e2873ed63fe2d3a6792191b154d", null ],
    [ "Scan_XYZ", "classFTPDriver_1_1TouchScreen.html#adba2644395adc1b0c8eddd41b93c55ca", null ],
    [ "Scan_Y", "classFTPDriver_1_1TouchScreen.html#afd05a6d7092626dfb3acaed8cadcd4f3", null ],
    [ "Scan_Z", "classFTPDriver_1_1TouchScreen.html#a9bfe4287742262244e689a43822d2e39", null ],
    [ "scan_tup", "classFTPDriver_1_1TouchScreen.html#a4fdc2c64ddd03c93f01106e814418ff2", null ],
    [ "X_convert", "classFTPDriver_1_1TouchScreen.html#a0be0d7cf82af801cbd706fbd0e243982", null ],
    [ "xc", "classFTPDriver_1_1TouchScreen.html#a65f77ba4b0376ec12fc13e2e1d5b6cd9", null ],
    [ "Y_convert", "classFTPDriver_1_1TouchScreen.html#a3cd8f605ed35effef2c7a34a2edcd9b6", null ],
    [ "yc", "classFTPDriver_1_1TouchScreen.html#a4cd216db077c3317b43ec067f2fc5645", null ]
];