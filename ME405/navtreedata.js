/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab0x00: Configuring Software", "page_UploadingToRepository.html", null ],
    [ "Lab0x01: Vendotron FSM", "page_VendotronFSM.html", null ],
    [ "Lab0x02: LED Reaction Game", "page_ThinkFast.html", null ],
    [ "Lab0x03: Pushing the Right Buttons", "page_StepResponse.html", null ],
    [ "Lab0x04: Temperature Sensor Data Storage", "page_HotorNot.html", null ],
    [ "Lab0x05: Rotating Platform Dynamics", "page_Lab5_Ball_6PlatformCalculations.html", null ],
    [ "Lab0x06: Simulink System Modeling", "page_Lab6_SystemModeling.html", [
      [ "Part 3a: All initial conditions set to 0", "page_Lab6_SystemModeling.html#sec_3a", null ],
      [ "Part 3b: Ball offset by distance of 5cm from center; all other conditions at 0", "page_Lab6_SystemModeling.html#sec_3b", null ],
      [ "Part 3c: Platform tilted at initial angle of 5 deg; all other conditions at 0", "page_Lab6_SystemModeling.html#sec_3c", null ],
      [ "Part 3d: Torque impulse of 1 mNm; all other conditions at 0", "page_Lab6_SystemModeling.html#sec_3d", null ],
      [ "Part 4: ClosedLoop Feedback System with Gain Coefficients [x=-.2 theta=-.3 x_der=-.05 theta_der=-.02]", "page_Lab6_SystemModeling.html#sec_4", null ]
    ] ],
    [ "Lab0x07: Getting Touchy", "page_TouchPanel.html", null ],
    [ "Lab0x08: Motor and Encoder Driver", "page_MotorEncoderDriver.html", null ],
    [ "Lab0x09: Platform Term Project", "page_TermProject.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Lab0__TestScript_8py.html",
"classME405__Drivers_1_1Encoder.html#a86b36e7823e05e76bf222cdaa815a605"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';