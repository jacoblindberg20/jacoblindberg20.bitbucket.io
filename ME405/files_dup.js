var files_dup =
[
    [ "Lab0_TestScript.py", "Lab0__TestScript_8py.html", "Lab0__TestScript_8py" ],
    [ "Lab1_VendotronFSM.py", "Lab1__VendotronFSM_8py.html", "Lab1__VendotronFSM_8py" ],
    [ "Lab2_ThinkFast.py", "Lab2__ThinkFast_8py.html", "Lab2__ThinkFast_8py" ],
    [ "Lab3_FrontEnd.py", "Lab3__FrontEnd_8py.html", "Lab3__FrontEnd_8py" ],
    [ "Lab3_Main.py", "Lab3__Main_8py.html", "Lab3__Main_8py" ],
    [ "Lab4_Main.py", "Lab4__Main_8py.html", "Lab4__Main_8py" ],
    [ "Lab4_mcp9808.py", "Lab4__mcp9808_8py.html", "Lab4__mcp9808_8py" ],
    [ "Lab7_FTPDriver.py", "Lab7__FTPDriver_8py.html", "Lab7__FTPDriver_8py" ],
    [ "Lab7_I2CDriver.py", "Lab7__I2CDriver_8py.html", [
      [ "BNO055", "classLab7__I2CDriver_1_1BNO055.html", "classLab7__I2CDriver_1_1BNO055" ]
    ] ],
    [ "Lab8_Main.py", "Lab8__Main_8py.html", "Lab8__Main_8py" ],
    [ "Lab8_MotorEncoderDriver.py", "Lab8__MotorEncoderDriver_8py.html", [
      [ "Encoder", "classLab8__MotorEncoderDriver_1_1Encoder.html", "classLab8__MotorEncoderDriver_1_1Encoder" ],
      [ "Motor", "classLab8__MotorEncoderDriver_1_1Motor.html", "classLab8__MotorEncoderDriver_1_1Motor" ]
    ] ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "ME405_Drivers.py", "ME405__Drivers_8py.html", [
      [ "Encoder", "classME405__Drivers_1_1Encoder.html", "classME405__Drivers_1_1Encoder" ],
      [ "Motor", "classME405__Drivers_1_1Motor.html", "classME405__Drivers_1_1Motor" ]
    ] ],
    [ "ME405_Lab7.py", "ME405__Lab7_8py.html", [
      [ "TouchScreen", "classME405__Lab7_1_1TouchScreen.html", "classME405__Lab7_1_1TouchScreen" ]
    ] ],
    [ "ME405_Lab8.py", "ME405__Lab8_8py.html", [
      [ "Encoder", "classME405__Lab8_1_1Encoder.html", "classME405__Lab8_1_1Encoder" ],
      [ "Motor", "classME405__Lab8_1_1Motor.html", "classME405__Lab8_1_1Motor" ]
    ] ],
    [ "ME405_main.py", "ME405__main_8py.html", "ME405__main_8py" ],
    [ "ME405_Touchscreen.py", "ME405__Touchscreen_8py.html", [
      [ "TouchScreen", "classME405__Touchscreen_1_1TouchScreen.html", "classME405__Touchscreen_1_1TouchScreen" ]
    ] ]
];