var Lab7__shares_8py =
[
    [ "Kp", "Lab7__shares_8py.html#a8e0fa35f348d2899497c4f43b6b8c93c", null ],
    [ "Kp_prime", "Lab7__shares_8py.html#ab3364d14733d4dc1de2e4009e9d82155", null ],
    [ "omega_act", "Lab7__shares_8py.html#a1a9ad824116d99fe07e9c636bcc0353d", null ],
    [ "omega_ref", "Lab7__shares_8py.html#a9f62ba2c8e103385f8a97bbe30fb29f8", null ],
    [ "position", "Lab7__shares_8py.html#ad1331e35321e0094c1ba43000215e742", null ],
    [ "prompt", "Lab7__shares_8py.html#a7af42360f3f756cf344ba9be2b84310c", null ],
    [ "time", "Lab7__shares_8py.html#ae5ff8ce8af7e059097f32efb74dd6dd2", null ],
    [ "time_delta", "Lab7__shares_8py.html#ab3e0913fbae108e432cca70c3d976e0a", null ],
    [ "velocity", "Lab7__shares_8py.html#a8afee792428fcfbc7e77e8cfd43ace64", null ]
];