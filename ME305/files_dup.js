var files_dup =
[
    [ "HW1_Elevator.py", "HW1__Elevator_8py.html", "HW1__Elevator_8py" ],
    [ "Lab1_fibonacci_sequence.py", "Lab1__fibonacci__sequence_8py.html", "Lab1__fibonacci__sequence_8py" ],
    [ "Lab2_main.py", "Lab2__main_8py.html", "Lab2__main_8py" ],
    [ "Lab2_mytest.py", "Lab2__mytest_8py.html", [
      [ "LED_Blinker", "classLab2__mytest_1_1LED__Blinker.html", "classLab2__mytest_1_1LED__Blinker" ]
    ] ],
    [ "Lab3_Encoder.py", "Lab3__Encoder_8py.html", [
      [ "Encoder", "classLab3__Encoder_1_1Encoder.html", "classLab3__Encoder_1_1Encoder" ]
    ] ],
    [ "Lab3_EncoderDriver.py", "Lab3__EncoderDriver_8py.html", [
      [ "EncoderDriver", "classLab3__EncoderDriver_1_1EncoderDriver.html", "classLab3__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab3_EncoderUI.py", "Lab3__EncoderUI_8py.html", [
      [ "EncoderUI", "classLab3__EncoderUI_1_1EncoderUI.html", "classLab3__EncoderUI_1_1EncoderUI" ]
    ] ],
    [ "Lab3_main.py", "Lab3__main_8py.html", "Lab3__main_8py" ],
    [ "Lab3_shares.py", "Lab3__shares_8py.html", "Lab3__shares_8py" ],
    [ "Lab4_main.py", "Lab4__main_8py.html", "Lab4__main_8py" ],
    [ "Lab4_UI_DataGen.py", "Lab4__UI__DataGen_8py.html", [
      [ "Data_Gen", "classLab4__UI__DataGen_1_1Data__Gen.html", "classLab4__UI__DataGen_1_1Data__Gen" ]
    ] ],
    [ "Lab4_UI_Front.py", "Lab4__UI__Front_8py.html", "Lab4__UI__Front_8py" ],
    [ "Lab5_BLE_Driver.py", "Lab5__BLE__Driver_8py.html", [
      [ "BLE_Driver", "classLab5__BLE__Driver_1_1BLE__Driver.html", "classLab5__BLE__Driver_1_1BLE__Driver" ]
    ] ],
    [ "Lab5_main.py", "Lab5__main_8py.html", "Lab5__main_8py" ],
    [ "Lab6_ClosedLoop.py", "Lab6__ClosedLoop_8py.html", [
      [ "ClosedLoop", "classLab6__ClosedLoop_1_1ClosedLoop.html", "classLab6__ClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Lab6_EncoderDriver.py", "Lab6__EncoderDriver_8py.html", [
      [ "EncoderDriver", "classLab6__EncoderDriver_1_1EncoderDriver.html", "classLab6__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab6_FrontEnd.py", "Lab6__FrontEnd_8py.html", "Lab6__FrontEnd_8py" ],
    [ "Lab6_Main.py", "Lab6__Main_8py.html", "Lab6__Main_8py" ],
    [ "Lab6_MotorDriver.py", "Lab6__MotorDriver_8py.html", [
      [ "MotorDriver", "classLab6__MotorDriver_1_1MotorDriver.html", "classLab6__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab6_Run.py", "Lab6__Run_8py.html", "Lab6__Run_8py" ],
    [ "Lab7_ClosedLoop.py", "Lab7__ClosedLoop_8py.html", [
      [ "ClosedLoop", "classLab7__ClosedLoop_1_1ClosedLoop.html", "classLab7__ClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Lab7_EncoderDriver.py", "Lab7__EncoderDriver_8py.html", [
      [ "EncoderDriver", "classLab7__EncoderDriver_1_1EncoderDriver.html", "classLab7__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab7_Main.py", "Lab7__Main_8py.html", "Lab7__Main_8py" ],
    [ "Lab7_MotorDriver.py", "Lab7__MotorDriver_8py.html", [
      [ "MotorDriver", "classLab7__MotorDriver_1_1MotorDriver.html", "classLab7__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab7_shares.py", "Lab7__shares_8py.html", "Lab7__shares_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ]
];