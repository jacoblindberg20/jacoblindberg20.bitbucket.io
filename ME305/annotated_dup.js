var annotated_dup =
[
    [ "HW1_Elevator", null, [
      [ "Button", "classHW1__Elevator_1_1Button.html", "classHW1__Elevator_1_1Button" ],
      [ "MotorDriver", "classHW1__Elevator_1_1MotorDriver.html", "classHW1__Elevator_1_1MotorDriver" ],
      [ "TaskElevator", "classHW1__Elevator_1_1TaskElevator.html", "classHW1__Elevator_1_1TaskElevator" ]
    ] ],
    [ "Lab2_mytest", null, [
      [ "LED_Blinker", "classLab2__mytest_1_1LED__Blinker.html", "classLab2__mytest_1_1LED__Blinker" ]
    ] ],
    [ "Lab3_Encoder", null, [
      [ "Encoder", "classLab3__Encoder_1_1Encoder.html", "classLab3__Encoder_1_1Encoder" ]
    ] ],
    [ "Lab3_EncoderDriver", null, [
      [ "EncoderDriver", "classLab3__EncoderDriver_1_1EncoderDriver.html", "classLab3__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab3_EncoderUI", null, [
      [ "EncoderUI", "classLab3__EncoderUI_1_1EncoderUI.html", "classLab3__EncoderUI_1_1EncoderUI" ]
    ] ],
    [ "Lab4_main", null, [
      [ "main", "classLab4__main_1_1main.html", "classLab4__main_1_1main" ]
    ] ],
    [ "Lab4_UI_DataGen", null, [
      [ "Data_Gen", "classLab4__UI__DataGen_1_1Data__Gen.html", "classLab4__UI__DataGen_1_1Data__Gen" ]
    ] ],
    [ "Lab5_BLE_Driver", null, [
      [ "BLE_Driver", "classLab5__BLE__Driver_1_1BLE__Driver.html", "classLab5__BLE__Driver_1_1BLE__Driver" ]
    ] ],
    [ "Lab6_ClosedLoop", null, [
      [ "ClosedLoop", "classLab6__ClosedLoop_1_1ClosedLoop.html", "classLab6__ClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Lab6_EncoderDriver", null, [
      [ "EncoderDriver", "classLab6__EncoderDriver_1_1EncoderDriver.html", "classLab6__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab6_Main", null, [
      [ "start", "classLab6__Main_1_1start.html", "classLab6__Main_1_1start" ]
    ] ],
    [ "Lab6_MotorDriver", null, [
      [ "MotorDriver", "classLab6__MotorDriver_1_1MotorDriver.html", "classLab6__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab6_Run", null, [
      [ "FSM", "classLab6__Run_1_1FSM.html", "classLab6__Run_1_1FSM" ]
    ] ],
    [ "Lab7_ClosedLoop", null, [
      [ "ClosedLoop", "classLab7__ClosedLoop_1_1ClosedLoop.html", "classLab7__ClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Lab7_EncoderDriver", null, [
      [ "EncoderDriver", "classLab7__EncoderDriver_1_1EncoderDriver.html", "classLab7__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab7_MotorDriver", null, [
      [ "MotorDriver", "classLab7__MotorDriver_1_1MotorDriver.html", "classLab7__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab7_Run", null, [
      [ "FSM", "classLab7__Run_1_1FSM.html", "classLab7__Run_1_1FSM" ]
    ] ]
];